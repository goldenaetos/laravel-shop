<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
//
//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

//Route::get('users', function(){
//   return 'users';
//});

//Route::post('users', function(){
//    return 'users';
//});

//Route::get('/tests', 'TestController@test'); - работает, т.к. не в папке Api.

//Route::get('/users', [UserController::class, 'index']);
//Route::get('/categories', [CategoryController::class, 'index']);
//Route::get('/products', [ProductController::class, 'index']);
//Route::get('/orders', [OrderController::class, 'index']);

//Route::apiResource('users', UserController::class, ['index']);


Route::apiResources([
    'users' => UserController::class,
    'categories' => CategoryController::class,
    'products' => ProductController::class,
    'orders' => OrderController::class

]);
